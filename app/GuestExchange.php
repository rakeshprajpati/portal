<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestExchange extends Model
{
    protected $table = 'guest_exchange_data';

    protected $fillable = ['recipient_name', 'email', 'message_to_admin', 'name', 'description'];

    
}
