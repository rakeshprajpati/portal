<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use Illuminate\Support\Facades\Storage;
use DB;
use App\User;


class SettingsController extends Controller
{

		  public function index(){

      $data = Settings::first();
    
			return view('pages.settings',compact('data'));

		}



		/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request->file('logo'));
        $this->validate($request, [
          'noticeboard' => 'required|string|max:2000',
        ]);


        $doc = Settings::find($request->input('setting_id'));
        if(empty($doc)){
        $doc = new Settings;
      }
        $doc->top_header_color                   = $request->input('top_header_color');
        $doc->top_header_logo_text               = $request->input('top_header_logo_text');
        $doc->dashboard_box_color                = $request->input('dashboard_box_color');
        $doc->sub_footer_color                   = $request->input('sub_footer_color');
        $doc->copyright_footer_color             = $request->input('copyright_footer_color');
        $doc->button_color                       = $request->input('button_color');
        $doc->button_hover_color                 = $request->input('button_hover_color');
        $doc->noticeboard_heading                = $request->input('noticeboard_heading');
        $doc->noticeboard                        = $request->input('noticeboard');
        $doc->noticeboard_color                  = $request->input('noticeboard_color');
      
         // handle file upload
          if ($request->hasFile('logo')) {

              $path =  $this->upload_file($request->file('logo'));

              $doc->logo = $path;

            }

            if ($request->hasFile('footerlogo')) {

              $path =  $this->upload_file($request->file('footerlogo'));

              $doc->footerlogo = $path;


             }
        
        // save to db
        $doc->save();
          

        return redirect('/dashboard/settings')->with('success','Settings Save');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


    public function upload_file($file){

        $dfile = mt_rand(1000, 9999);

    	 // handle file upload
        if ($file) {
            // filename with extension
            $fileNameWithExt = $file->getClientOriginalName();
            // filename
            $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // extension
            $extension = $file->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // upload file
            $path = $file->storeAs('public/files/'.$dfile, $fileNameToStore);
           }
			   return $path;
    	}


      public static function getSettings(){


        $data = Settings::first();

        // foreach ($data as $key => $value) {

        //   return $value->id;
        // }
       
        return $data;

      } 


}