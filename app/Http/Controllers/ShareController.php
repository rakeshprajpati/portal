<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shared;
use App\Document;
use App\User;
use App\Category;

use Illuminate\Support\Facades\Storage;
use Mail;


class ShareController extends Controller
{
    public function __construct() {
        return $this->middleware(['auth','permission:shared']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	if (auth()->user()->hasAnyRole('Root','Admin'))
        {
        	$shared = Shared::all();

        	return view('pages.shared',compact('shared'));
    	}
        elseif (auth()->user()->hasRole('Admin')){

            $shared = Shared::all();

            //dd($shared);

            return view('pages.shared',compact('shared'));

        }
        else{

    		return redirect('/documents');

    	}
    }

 
        public function open($id)
    {
        $doc = Shared::findOrFail($id);
        $path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($doc->file);
        $type = $doc->mimetype;

        \Log::addToLog('Document ID '.$id.' was viewed');

        if ($type == 'application/pdf' || $type == 'image/jpeg' ||
        $type == 'image/png' || $type == 'image/jpg' || $type == 'image/gif')
        {
            return response()->file($path, ['Content-Type' => $type]);
        }
        elseif ($type == 'video/mp4' || $type == 'audio/mpeg' ||
        $type == 'audio/mp3' || $type == 'audio/x-m4a')
        {
            return view('documents.play',compact('doc'));
        }
        else {
            return response()->file($path, ['Content-Type' => $type]);
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name','id')->all();

        return view('documents.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
          'name' => 'required|string|max:255',
          //'description' => 'required|string|max:255',
          'file' => 'required|max:50000',
        ]);

        $shared_user = User::findOrFail($request->input('hidden'));

	       // get the data of uploaded user
	        $user_id 		= auth()->user()->id;
	        $department_id 	= auth()->user()->department_id;

        // handle file upload
        if ($request->hasFile('file')) {
            // filename with extension
          foreach ($request->file('file') as $value) {
          
            $fileNameWithExt = $value->getClientOriginalName();
            // filename
            $filename      = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // extension
            $extension     = $value->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // upload file
            $path        = $value->storeAs('public/files/'.$user_id, $fileNameToStore);

            $filePath[]    = $path;
            
            $fmimeType[]   = Storage::mimeType($path);

            $size = Storage::size($path);
            if ($size >= 1000000) {
              $fileSizes[]  = round($size/1000000) . 'MB';
            }elseif ($size >= 1000) {
              $fileSizes[]  = round($size/1000) . 'KB';
            }else {
              $fileSizes[]  = $size;
            }


            }

        }



        $shared = new Shared;
        $shared->name           = $request->input('name');
        $shared->description    = $request->input('description');
        $shared->document_id    = 0;
        $shared->user_id        = $user_id;
        $shared->share_with_user= $shared_user->id;
        $shared->department_id  = $department_id;
        $shared->file           = json_encode($filePath);
        // $doc->mimetype = Storage::mimeType($path);
        $shared->mimetype       = json_encode($fmimeType);
        $shared->filesize       = json_encode($fileSizes);
        // determine whether it expires
        if ($request->input('isExpire') == true) {
            $shared->isExpire = false;
        }else {
            $shared->isExpire = true;
            $shared->expires_at = $request->input('expires_at');
        }
        $shared->save();
	$shared->categories()->sync($request->category_id);


        \Log::addToLog('Document name '.$shared->name.' was shared with user '.$shared_user->name);

        // to send email file details

            $data  =  array(  
                                'filename'  =>  ucfirst($request->input('name')),
                                'user'      =>  ucfirst($shared_user->name),
                                'email'     =>  $shared_user->email,
                            );

            Mail::send('emails.shared', $data, function($message) use ($data) {

            $message->to($data['email'])->subject('Hi! '.$data['user'].' OrangeSockPay shared a file.');
          });

        return redirect('/shared')->with('success','File Shared!');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $doc = Document::findOrFail($id);

        $shared = new Shared;
        $shared->name = $doc->name;
        $shared->description = $doc->description;
        $shared->document_id = $doc->id;
        $shared->user_id = $doc->user_id;
        $shared->department_id = $doc->department_id;
        $shared->file = $doc->file;
        $shared->mimetype = $doc->mimetype;
        $shared->filesize = $doc->filesize;
        $shared->isExpire = $doc->isExpire;
        $shared->expires_at = $doc->expires_at;
        $shared->save();

        \Log::addToLog('Document ID '.$id.' was shared');

        return redirect('/documents')->with('success','File Shared!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $doc = Shared::findOrFail($id);
       // delete the file on disk
       $files = json_decode($doc->file);
       foreach ($files as $file) {
        Storage::delete($file);
       }
       // delete db record
        $doc->delete();

        \Log::addToLog('Document ID '.$id.' was deleted');

        return redirect()->back()->with('success','Deleted!');
    }

    /**
     * Store and share a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function upload($id)
    {
         $user = User::findOrFail($id);
	$categories = Category::pluck('name','id')->all();

        return view('share.upload',compact('user','categories'));
    }

        /**
     * Download a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function DownloadFiles(Request $request){

        $download_code = $request->input('id');

        $downloads = Shared::where('id',$download_code)->get();

            if (!$downloads->isEmpty()) { 
              
                  $path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($downloads[0]->file);
        
                   return response()->download($path);
                  }

    		}

    public function adminUserSharedDocs($id){

            $shared_user = User::findOrFail($id);

                  // get user's docs
            // $user_id = auth()->user()->id;
            $userid = $shared_user->id;

            $shared = Shared::where('share_with_user',$userid)->get();

            // get docs in dept
            // $dept_id = auth()->user()->department_id;

            $userdocs = Document::where('isExpire','!=',2)->where('user_id','=',$userid)->get();
            $filetype = null;

        return view('share.adminuserdoc',compact('userdocs','filetype','shared','userid'));



    }
}
