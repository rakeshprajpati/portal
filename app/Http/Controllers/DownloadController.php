<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\GuestExchange;
use App\Shared;
use App\Document;
use Illuminate\Support\Facades\Storage;
use DB;
use Mail;
use App\User;


class DownloadController extends Controller
{
    
    public function guest_multi_down($id){

    	$finalArrayfile =0;
        $downloads = GuestExchange::where('id',$id)->get();

        $name = $downloads[0]->name;
        $files = json_decode($downloads[0]->file);
        $filesizes = json_decode($downloads[0]->filesize);
		
		if(is_array($files) && is_array($filesizes)){

           $finalArrayfile = array_combine($files, $filesizes);

            }else{

        	$files     = json_decode(json_encode($downloads[0]->file));
        	$filesizes = json_decode(json_encode($downloads[0]->filesize));

        	$finalArrayfile = array($files=>$filesizes);

         }

        return view('pages.multi_file',compact('name','finalArrayfile'));

        }

        public function shared_multi_down($id){

        $finalArrayfile =0;
        $downloads = Shared::where('id',$id)->get();

        $name = $downloads[0]->name;
        
        $files = json_decode($downloads[0]->file);
        
        $filesizes = json_decode($downloads[0]->filesize);
        
        if(is_array($files) && is_array($filesizes)){

         $finalArrayfile = array_combine($files, $filesizes);

            }else{

            $files     = json_decode(json_encode($downloads[0]->file));
            
            $filesizes = json_decode(json_encode($downloads[0]->filesize));

            $finalArrayfile = array($files=>$filesizes);

          }

          return view('pages.multi_file',compact('name','finalArrayfile'));

        }

        public function document_multi_down($id){

        $finalArrayfile =0;

        $downloads = Document::where('id',$id)->get();

            $name = $downloads[0]->name;

            $files = json_decode($downloads[0]->file);

            $filesizes = json_decode($downloads[0]->filesize);
            
            if(is_array($files) && is_array($filesizes)){

              $finalArrayfile = array_combine($files, $filesizes);

                }else{

            $files     = json_decode(json_encode($downloads[0]->file));
            $filesizes = json_decode(json_encode($downloads[0]->filesize));

            $finalArrayfile = array($files=>$filesizes);

          }
          return view('pages.multi_file',compact('name','finalArrayfile'));
        }
  }