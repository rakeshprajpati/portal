<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GuestExchange;
use Illuminate\Support\Facades\Storage;
use DB;
use Mail;
use App\User;


class GuestExchangeController extends Controller
{

		public function index(){
    
			return view('guestexchange.upload');

		}

    public function downloads(){
    
      return view('guestexchange.download');

    }


     /**
     * Download a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function DownloadFiles(Request $request){
      $this->validate($request, ['download_code' => 'required|string|max:25']);

       $download_code = $request->input('download_code');
        $finalArrayfile =0;


      $downloads = GuestExchange::where('download_code',$download_code)->get();

                  if (!$downloads->isEmpty()){ 
              
                  //$path = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($downloads[0]->file);
        
                  // return response()->download($path);

                    $name = $downloads[0]->name;

                $files = json_decode($downloads[0]->file);

                $filesizes = json_decode($downloads[0]->filesize);
                
                if(is_array($files) && is_array($filesizes)){

                  $finalArrayfile = array_combine($files, $filesizes);

                    }else{

                $files     = json_decode(json_encode($downloads[0]->file));
                $filesizes = json_decode(json_encode($downloads[0]->filesize));

                $finalArrayfile = array($files=>$filesizes);

                }
                  return view('pages.multi_file',compact('name','finalArrayfile'));

                  }
                  else{

                   return redirect()->back()->withErrors('Current Download code is incorrect!');
                  }



    }

		/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'recipient_name' => 'required|string|max:255',
          'email' => 'required|string|email|max:255',
          'name' => 'required|string|max:255',
          'description' => 'required|string|max:255',
          'file' => 'required|max:50000',
        ]);

        // // get the data of uploaded user
        // $user_id = auth()->user()->id;
        // $department_id = auth()->user()->department_id;
        $dfile = mt_rand(1000, 9999);
        $download_digit = mt_rand(100000, 999999);


        // handle file upload
        if ($request->hasFile('file')) {
            // filename with extension
          foreach ($request->file('file') as $value) {
          
            $fileNameWithExt = $value->getClientOriginalName();
            // filename
            $filename      = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // extension
            $extension     = $value->getClientOriginalExtension();
            // filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // upload file
            $path        = $value->storeAs('public/files/'.$dfile, $fileNameToStore);

            $filePath[]    = $path;
            
            $fmimeType[]   = Storage::mimeType($path);

            $size = Storage::size($path);
            if ($size >= 1000000) {
              $fileSizes[]  = round($size/1000000) . 'MB';
            }elseif ($size >= 1000) {
              $fileSizes[]  = round($size/1000) . 'KB';
            }else {
              $fileSizes[]  = $size;
            }


            }

        }

        $doc = new GuestExchange;
        $doc->recipient_name    = $request->input('recipient_name');
        $doc->email             = $request->input('email');
        $doc->message_to_admin  = $request->input('message_to_admin');
        $doc->name              = $request->input('name');
        $doc->description       = $request->input('description');
        $doc->download_code     = $download_digit;
      
        // $doc->file = $path;
        $doc->file        = json_encode($filePath);
        // $doc->mimetype = Storage::mimeType($path);
        $doc->mimetype      = json_encode($fmimeType);
        // $size = Storage::size($path);
        // if ($size >= 1000000) {
        //   $doc->filesize = round($size/1000000) . 'MB';
        // }elseif ($size >= 1000) {
        //   $doc->filesize = round($size/1000) . 'KB';
        // }else {
        //   $doc->filesize = $size;
        // }
        $doc->filesize = json_encode($fileSizes);
        // determine whether it expires
        if ($request->input('isExpire') == true) {
            $doc->isExpire = false;
        }else {
            $doc->isExpire = true;
            $doc->expires_at = $request->input('expires_at');
        }
        // save to db
        $doc->save();

        \Log::addToLog('New Guest Document, '.$request->input('name').' was uploaded');

            $data  =  array(  
                              'download_code'      =>  $download_digit,
                               'file_name'         =>  ucfirst($request->input('name')),
                               'recipient_name'    =>  ucfirst($request->input('recipient_name')),
                               'email'             =>  $request->input('email'),
                            );
          // to send email files details
            Mail::send('emails.downloadcode', $data, function($message) use ($data) {
           // $message->from('ve.rakesh87@gmail.com', 'BOA');
            $message->to($data['email'])->subject('Hi! '.$data['file_name'].' File is Shared with You');
          });


        return redirect('/guest_exchange/upload')->with('success','Email Sent & File Uploaded');
    }

      // List the all guest document to admin/root user
    public function listDocuments(){

         if (auth()->user()->hasRole('Root|Admin'))
        {
            // get all
            $docs = GuestExchange::where('isExpire','!=',2)->orderBy('created_at', 'desc')->get();
        }
                $filetype = null;

          return view('guestexchange.list',compact('docs','filetype'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = GuestExchange::findOrFail($id);
        // delete the file on disk
        $files = json_decode($doc->file);
        foreach($files as $file){
        Storage::delete($file);
      }
        // delete db record
        $doc->delete();


        \Log::addToLog('Document ID '.$id.' was deleted');

        return redirect('/guest_exchange/list')->with('success','Deleted!');
    }


    public static function strip_tags_func($string, $len){
    	// strip tags to avoid breaking any html
			$string = strip_tags($string);
			if (strlen($string) > $len) {

			    // truncate string
			    $stringCut = substr($string, 0, $len);
			    $endPoint = strrpos($stringCut, ' ');

			    //if the string doesn't contain any space then it will cut without word basis.
			    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
			    $string .= '...';
			}
			return $string;
    	}

    	// delete multiple docs selected
    public function deleteMulti(Request $request)
    {
      $ids = $request->ids;
      DB::table('guest_exchange_data')->whereIn('id',explode(',', $ids))->delete();

      \Log::addToLog('Selected Documents Are Deleted!');

      return redirect('/guest_exchange/list')->with('success','Selected Documents Deleted!');
    }



}