<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
// for displaying data
use App\Department;
use App\Document;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
        
        return $this->middleware(['auth','permission:manage|edit|read']);
        //return view('documents/index');

      

     }


    public function index()
    { 
            // dd($this);

        // $users = User::where('status',true)->get();
        if (auth()->user()->hasRole('Root'))
        {
          $users = User::where('status',true)->get()->count();
        }
        elseif (auth()->user()->hasRole('Admin'))
        {
          $d = auth()->user()->department_id;
          // $users = User::where('status',true)->where('department_id',$d)->where('id','!=',auth()->user()->id)->get()->count();
          $users = User::where('status',true)->get()->count();

        }
        else
        {
          return redirect('/documents');
        }
        // get all dept
        $docs = Document::all()->count();
        // get roles
        if (auth()->user()->hasRole('Root'))
        {
          $roles = Role::where('name','!=','Root')->get()->count();
        }
        else
        {
          $roles = Role::where('name','!=','Root')->where('name','!=','Admin')->get()->count();
        }

        return view('dashboard',compact('users','docs','roles'));
    }
}
