<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = ['name','user_id'];

    public function documents() {
        return $this->belongsToMany('App\Document');
    }
    public function Shared() {
        return $this->belongsToMany('App\Shared');
    }

}
