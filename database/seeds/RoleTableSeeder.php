<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;


class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = Role::create(['name' => 'Root'])->syncPermissions(['root', 'manage','read','edit','delete','upload','download','shared']);
        $r2 = Role::create(['name' => 'Admin'])->syncPermissions(['manage','read','edit','delete','upload','download','shared']);
        $r3 = Role::create(['name' => 'User'])->syncPermissions(['read','edit','delete','upload','download','shared']);

        // $r1->givePermissionTo(Permission::all());

        // $role = Role::create(['name' => 'super-admin']);
        // $role->givePermissionTo(Permission::all());


    }
}
