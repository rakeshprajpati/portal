<?php

use Illuminate\Database\Seeder;
use App\Settings;
class SettingTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $s = Settings::create(['top_header_color' => '#ef7b35',
                                'top_header_logo_text' => 'Secure Send',
                                'dashboard_box_color' => '#ef7b35',
                                'sub_footer_color' => '#ef7b35',
                                'copyright_footer_color' => '#ef7b35',
                                'button_color' => '#E9C937',
                                'button_hover_color' => '#E9C937',
                                'logo' => '/',
                                'footerlogo' => '/',
                                'noticeboard_heading' => 'Notice',
                                'noticeboard' => 'Notice Board',
                                'noticeboard_color' => '#ef7b35'

            ]);

    }
}
