<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name  = $_ENV['FORGE_API_NAME'];
        $email = $_ENV['FORGE_API_EMAIL'];
        $pass  = $_ENV['FORGE_API_PASS'];

        $root = new User();
        $root->name = 'Rakesh Prajapati';
        $root->email = 'developer@askaffinity.com';
        $root->password = 'developer@';
        $root->status = true;
        $root->save();

        if($email){

        $admin = new User();
        $admin->name = $name;
        $admin->email = $email;
        $admin->password = $pass;
        $admin->department_id = '1';
        $admin->status = true;
        $admin->save();
        }
        
        $user = new User();
        $user->name = 'Steve Rogers';
        $user->email = 'captain@gmail.com';
        $user->password = 'captain';
        $user->department_id = '2';
        $user->status = true;
        $user->save();

//        $r1 = Role::where('name','Root')->first();
        //$r2 = Role::where('name','Admin')->first();
        //$r3 = Role::where('name','User')->first();

        $root->syncRoles( array('Root'));
        $admin->syncRoles( array('Admin'));
        $user->syncRoles( array('User'));

        // $admin->assignRole($r2);

        // $user->assignRole($r3);


    }
}
