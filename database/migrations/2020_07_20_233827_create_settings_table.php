<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');

            $table->string('top_header_color');
            $table->string('top_header_logo_text');

            $table->string('dashboard_box_color');
            $table->string('sub_footer_color');
            $table->string('copyright_footer_color');

            $table->string('button_color');
            $table->string('button_hover_color');

            $table->string('logo');
            
            $table->string('footerlogo');
            
            $table->string('noticeboard_heading');
            $table->longText('noticeboard');
            $table->string('noticeboard_color');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
           Schema::dropIfExists('settings');
    }
}
