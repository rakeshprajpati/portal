<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestExchangeData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('guest_exchange_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('recipient_name');
            $table->string('email');
            $table->string('message_to_admin');
            $table->string('name');
            $table->string('description');
            $table->longText('file');
            $table->string('filesize');
            $table->string('mimetype');
            $table->boolean('isExpire')->default(0);
            $table->date('expires_at')->nullable();
            $table->string('download_code')->nullable();
            $table->timestamps();
        });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guest_exchange_data');
    }
}
