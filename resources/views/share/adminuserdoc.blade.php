@extends('layouts.app')

@section('content')
<style>
  td form{
    float:right;
  }
  .card-content2 {
    padding: 10px 7px;
  }
  /* --- for right click menu --- */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
  .task i {
    color: orange;
    font-size: 35px;
  }
  /* context-menu */
  .context-menu {
    padding: 0 5px;
    margin: 0;
    background: #f7f7f7;
    font-size: 15px;
    display: none;
    position: absolute;
    z-index: 10;
    box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
  }
  .context-menu--active {
    display: block;
  }
  .context-menu_items {
    margin: 0;
  }
  .context-menu_item {
    border-bottom: 1px solid #ddd;
    padding: 12px 30px;
  }
  .context-menu_item:last-child {
    border-bottom: none;
  }
  .context-menu_item:hover {
    background: #fff;
  }
  .context-menu_item i {
    margin: 0;
    padding: 0;
  }
  .context-menu_item p {
    display: inline;
    margin-left: 10px;
  }
  .unshow {
    display: none;
  }
</style>
@hasanyrole('Root|Admin')
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    <div class="col m11 s12">
      <div class="row">
        <h3 class="flow-text"><i class="material-icons">folder</i> To Preparer
          <button class="btn red waves-effect waves-light right tooltipped delete_all" data-url="{{ url('documentsDeleteMulti') }}" data-position="left" data-delay="50" data-tooltip="Delete Selected Documents"><i class="material-icons">delete</i></button>
        @can('upload')
          <a href="{{ url('/') }}/share/{{$userid}}/upload" class="btn waves-effect waves-light right tooltipped" data-position="left" data-delay="50" data-tooltip="Upload New Document"><i class="material-icons">file_upload</i></a>
        @endcan
        </h3>
        <div class="divider"></div>
      </div>
      <div class="card z-depth-2">
        <div class="card-content">
          <!-- Switch -->
          <!-- TABLE View -->
          <div id="tableView" class="show">
            <div class="row">
              <table class="bordered centered highlight responsive-table" id="myDataTable">
                <thead>
                  <tr>
                      <th></th>
                      <th>File Name</th>
                      <th>Category</th>
                      <th>Owner</th>
                      <th>Department</th>
                      <th>Uploaded At</th>
                      <th>Expires At</th>
                      <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($shared) > 0 && !empty($shared))
                    @foreach($shared as $doc)
                    <tr id="tr_{{$doc->id}}">
                      <td>
                        <input type="checkbox" id="chk_{{ $doc->id }}" class="sub_chk" data-id="{{$doc->id}}">
                        <label for="chk_{{ $doc->id }}"></label>
                      </td>
                      <td>{{ $doc->name }}</td>
                          <td>
                            @if(!empty($doc->categories()->get()))
					    @php $comma= (array) null; @endphp
                                              @foreach($doc->categories()->get() as $cate)
                                               @php $comma[] = $cate->name @endphp 
                                              @endforeach
                                    {!! !empty($comma) ? implode(', ', $comma) : 'Category not exists!' !!}   
                            @endif
                            </td>

                      <td>{!! !empty($doc->user->name) ? $doc->user->name : 'User not exist' !!}</td>
                  <td>{!! !empty($doc->user->department['dptName']) ? $doc->user->department['dptName'] : 'Department not exist' !!}</td>
                  
                      <td>{{ $doc->created_at->toDayDateTimeString() }}</td>
                      <td>
                        @if($doc->isExpire)
                          {{ $doc->expires_at }}
                        @else
                          No Expiration
                        @endif
                      </td>
                      <td>

                        @can('download')

                        <a href="{{ url('/') }}/share/download/{{ $doc->id }}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Download"><i class="material-icons">file_download</i></a>
                        @endcan
                        <!-- DELETE using link -->
                        {!! Form::open(['action' => ['ShareController@destroy', $doc->id],
                        'method' => 'DELETE', 'id' => 'form-delete-documents-' . $doc->id]) !!}
                        @can('delete')
                        <a href="" class="data-delete tooltipped" data-position="left" data-delay="50" data-tooltip="Delete" data-form="documents-{{ $doc->id }}"><i class="material-icons">delete</i></a>
                        @endcan
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="6"><h5 class="teal-text">Document Not Available</h5></td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <div class="section">
    <div class="col m1 hide-on-med-and-down">
<!--       include('inc.sidebar')
 -->    </div>
    <div class="col m11 s12">
      <div class="row">
        <h3 class="flow-text"><i class="material-icons">folder</i> From Preparer
        </h3>
        <div class="divider"></div>
      </div>
      <div class="card z-depth-2">
        <div class="card-content">
          <!-- Switch -->
          
          <!-- TABLE View unshow-->
          <div id="tableView" class="show">
            <div class="row">
              <table class="bordered centered highlight responsive-table" id="myDataTable1">
                <thead>
                  <tr>
                      <th></th>
                      <th>File Name</th>
                      <th>Category</th>
                      <th>Owner</th>
                      <th>Department</th>
                      <th>Uploaded At</th>
                      <th>Expires At</th>
                      <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($userdocs) > 0 && !empty($userdocs))
                    @foreach($userdocs as $doc)
                    <tr id="tr_{{$doc->id}}">
                      <td>
                        <input type="checkbox" id="chk_{{ $doc->id }}" class="sub_chk" data-id="{{$doc->id}}">
                        <label for="chk_{{ $doc->id }}"></label>
                      </td>
                      <td>{{ $doc->name }}</td>
                          <td>
                          @if(!empty($doc->categories()->get()))
					@php $comma= (array) null; @endphp
                                          @foreach($doc->categories()->get() as $cate)
                                             @php $comma[] = $cate->name @endphp 
                                            @endforeach
                                  {!! !empty($comma) ? implode(', ', $comma) : 'Category not exists!' !!}   
                          @endif
                          </td>

                      <td>{!! !empty($doc->user->name) ? $doc->user->name : 'User not exist' !!}</td>
                  <td>{!! !empty($doc->user->department['dptName']) ? $doc->user->department['dptName'] : 'Department not exist' !!}</td>
                  
                      <td>{{ $doc->created_at->toDayDateTimeString() }}</td>
                      <td>
                        @if($doc->isExpire)
                          {{ $doc->expires_at }}
                        @else
                          No Expiration
                        @endif
                      </td>
                      <td>

                        @can('read')
                        {!! Form::open() !!}
                        <a href="{{ url('/') }}/documents/{{ $doc->id }}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="View Details"><i class="material-icons">visibility</i></a>
                        {!! Form::close() !!}
                        <!-- {!! Form::open() !!}
                        <a href="{{ url('/') }}/documents/open/{{ $doc->id }}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Open"><i class="material-icons">open_with</i></a>
                        {!! Form::close() !!} -->
                        @endcan
                        {!! Form::open() !!}
                        @can('download')
                        <a href="{{ url('/') }}/document/download/{{ $doc->id }}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Download"><i class="material-icons">file_download</i></a>
                        @endcan
                        {!! Form::close() !!}
                        <!-- SHARE using link -->
                        {!! Form::open(['action' => ['ShareController@update', $doc->id], 'method' => 'PATCH', 'id' => 'form-share-documents-' . $doc->id]) !!}
                        @can('shared')
                        <a href="" class="data-share tooltipped" data-position="left" data-delay="50" data-tooltip="Share" data-form="documents-{{ $doc->id }}"><i class="material-icons">share</i></a>
                        @endcan
                        {!! Form::close() !!}
                        {!! Form::open() !!}
                        @can('edit')
                        <a href="{{ url('/') }}/documents/{{ $doc->id }}/edit" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Edit"><i class="material-icons">mode_edit</i></a>
                        @endcan
                        {!! Form::close() !!}
                        <!-- DELETE using link -->
                        {!! Form::open(['action' => ['DocumentsController@destroy', $doc->id],
                        'method' => 'DELETE', 'id' => 'form-delete-documents-' . $doc->id]) !!}
                        @can('delete')
                        <a href="" class="data-delete tooltipped" data-position="left" data-delay="50" data-tooltip="Delete" data-form="documents-{{ $doc->id }}"><i class="material-icons">delete</i></a>
                        @endcan
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="6"><h5 class="teal-text">Document Not Available</h5></td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>

@endhasanyrole
@endsection
