@extends('layouts.app')

@section('content')
  <!-- Admin Dashboard -->
  <div class="container">
    <div class="row">
      <div class="col s12">
        <h3 class="flow-text"><i class="material-icons">settings</i> Dashboard</h3>
        <div class="divider"></div>
      </div>
      <div class="section">
        <a href="{{ url('/') }}/documents">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">folder</i></p>
                <h4 class="center-align flow-text">Documents<span class="new badge white-text">{{$docs}}</span></h4>
              </div>
            </div>
          </div>
        </a>
        @hasanyrole('Root|Admin')
        <a href="{{ url('/') }}/users">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">person</i></p>
                <h4 class="center-align flow-text">Users<span class="new badge white-text">{{ $users }}</span></h4>
              </div>
            </div>
          </div>
        </a>
        <a href="{{ url('/') }}/departments">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">group</i></p>
                <h4 class="center-align flow-text">Departments</h4>
              </div>
            </div>
          </div>
        </a>
        @hasanyrole('Root')

        <a href="{{ url('/') }}/roles">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">assignment_ind</i></p>
                <h4 class="center-align flow-text">Roles &amp; Permissions</h4>
              </div>
            </div>
          </div>
        </a>
        <a href="{{ url('/') }}/logs">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">view_list</i></p>
                <h4 class="center-align flow-text">Logs</h4>
              </div>
            </div>
          </div>
        </a>
        @endhasanyrole

        <a href="{{ url('/') }}/categories">
          <div class="col m6 s6">
            <div class="card hoverable indigo lighten-1 white-text">
              <div class="card-content">
                <p class="center"><i class="large material-icons">border_all</i></p>
                <h4 class="center-align flow-text">Categories</h4>
              </div>
            </div>
          </div>
        </a>
            @endhasanyrole

      </div>
    </div>
  </div>
@endsection
