@if( \App\Http\Controllers\SettingsController::getSettings() && \App\Http\Controllers\SettingsController::getSettings()->sub_footer_color )
<footer class="page-footer" style="background-color:{{ \App\Http\Controllers\SettingsController::getSettings()->sub_footer_color }}">
  @else
<footer class="page-footer indigo darken-11">
@endif
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <!-- <h5 class="white-text">Affinity Solutions</h5>
        <p class="grey-text text-lighten-4">Back Office Administration</p> -->
@if( \App\Http\Controllers\SettingsController::getSettings() )
       <a href="{{ url('/') }}"><img width="175px" src="{{ url(Storage::url( \App\Http\Controllers\SettingsController::getSettings()->footerlogo )) }}" alt="{{ \App\Http\Controllers\SettingsController::getSettings()->top_header_logo_text }}"></a>
@endif
      </div>
    </div>
  </div>
  @if( \App\Http\Controllers\SettingsController::getSettings() && \App\Http\Controllers\SettingsController::getSettings()->copyright_footer_color )
  <div class="footer-copyright" style="background-color:{{ \App\Http\Controllers\SettingsController::getSettings()->copyright_footer_color }}">
    @else
  <div class="footer-copyright indigo darken-4">
    @endif
    <div class="container">
      Copyright {{ Date('Y') }} © @if( \App\Http\Controllers\SettingsController::getSettings() ) 
      {{ \App\Http\Controllers\SettingsController::getSettings()->top_header_logo_text }}
      @endif
 | Designed by <a style="color:#000;" href="https://www.askaffinity.com/" target="_blank">Affinity Solutions</a>
    <a class="grey-text text-lighten-4 right modal-trigger" href="#modal2">Help</a>
    </div>
  </div>
</footer>
<!-- Modal For Help -->
<div id="modal2" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Help</h4>
    <p>
      @if( \App\Http\Controllers\SettingsController::getSettings() ) 
      {{ \App\Http\Controllers\SettingsController::getSettings()->top_header_logo_text }} 
      @endif

      Support Help Team! 
    </p>
    <p><strong>For any query or support, please contact us at the emails below: </strong></p>
    <p>1. <a href="mailto:developer@askaffinity.com">developer@askaffinity.com</a></p>
    <p>2. <a href="mailto:info@askaffinity.com">info@askaffinity.com</a></p>

  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">OK</a>
  </div>
</div>
@if( \App\Http\Controllers\SettingsController::getSettings() ) 
<style type="text/css">
@if( \App\Http\Controllers\SettingsController::getSettings()->button_color )
  .btn, .btn-large{ background-color: {{ ( \App\Http\Controllers\SettingsController::getSettings()->button_color ) }} !important;  }
  .btn:hover, .btn-large:hover{ background-color: {{ ( \App\Http\Controllers\SettingsController::getSettings()->button_hover_color ) }} !important;  }
span.badge.new {
background-color: {{ ( \App\Http\Controllers\SettingsController::getSettings()->button_color ) }} !important;
}

@endif

@if( \App\Http\Controllers\SettingsController::getSettings()->noticeboard_color )
 div.card-panel.teal{ background-color: {{ ( \App\Http\Controllers\SettingsController::getSettings()->noticeboard_color ) }} !important;  }
@endif

@if( \App\Http\Controllers\SettingsController::getSettings()->dashboard_box_color )
.indigo.lighten-1{ background-color: {{ ( \App\Http\Controllers\SettingsController::getSettings()->dashboard_box_color ) }} !important;  }
@endif
</style>
@endif
<!-- Start of LiveChat (www.livechatinc.com) code -->
<!-- End of LiveChat code -->