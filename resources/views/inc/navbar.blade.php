<nav>

  @if( \App\Http\Controllers\SettingsController::getSettings() && \App\Http\Controllers\SettingsController::getSettings()->top_header_color )
  <div class="nav-wrapper" style="background-color:{{ \App\Http\Controllers\SettingsController::getSettings()->top_header_color }} ">
  @else
  <div class="nav-wrapper indigo darken-4">
  @endif
   @if( \App\Http\Controllers\SettingsController::getSettings() )
    <a href="{{  url('/')  }}" class="brand-logo center">
    	@if(  \App\Http\Controllers\SettingsController::getSettings()->logo )

    	<img width="75px" title="OrangeSock - Dashboard" src="{{ url(Storage::url( \App\Http\Controllers\SettingsController::getSettings()->logo )) }}" alt="{{ \App\Http\Controllers\SettingsController::getSettings()->top_header_logo_text }}">

    	@else

      <i class="material-icons backup">backup</i>{{ \App\Http\Controllers\SettingsController::getSettings()->top_header_logo_text }}

       @endif
    </a>
    @endif
    <a href="#" data-activates="mobile-demo" class="button-collapse">
      <i class="material-icons">menu</i>
    </a>
    <!-- Mobile View -->
    <ul class="side-nav" id="mobile-demo">
      @if(Auth::guest())
        <li><a href="{{ route('login') }}">Login</a></li>
        <li><a href="{{ route('register') }}">Register</a></li>
      @else
        <!-- <li><a href="/shared">Shared</a></li> -->
        <li><a href="{{ url('/') }}/documents">Documents</a></li>
        <li><a href="{{ url('/') }}/mydocuments">My Documents</a></li>
        <li><a href="{{ url('/') }}/categories">Categories</a></li>
        @hasanyrole('Root|Admin')
        <li><a href="{{ url('/') }}/users">Users</a></li>
        <li><a href="{{ url('/') }}/departments">Departments</a></li>
        <li><a href="{{ url('/') }}/logs">Logs</a></li>
        @hasrole('Root')
        <li><a href="{{ url('/') }}/backup">Backup</a></li>
        @endhasrole
        @endhasanyrole
        <li class="divider"></li>
        <li><a href="{{ url('/') }}/profile">My Account</a></li>
        <li>
            <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
      @endif
    </ul>
    <!-- Desktop View -->
    <ul class="right hide-on-med-and-down">
      <!-- Authentication Links -->
      @if (Auth::guest())
        <li><a href="{{ route('login') }}">Login</a></li>
        <li><a href="{{ route('register') }}">Register</a></li>
      @else
        <!-- Dropdown Trigger -->

        {{-- <li>
          <a href="" class="datepicker"><i class="material-icons">date_range</i></a>
        </li> --}}
        
        <li>
          @if(!empty($trashfull) &&  $trashfull > 0)
          <a href="{{ url('/') }}/trash"><i class="material-icons red-text">delete</i></a>
          @else
          <a href="{{ url('/') }}/trash"><i class="material-icons">delete</i></a>
          @endif
        </li>
        @hasanyrole('Root|Admin')
        <li>
          <a href="{{ url('/') }}/requests">Requests<span class="new badge white-text">{{ $requests }}</span></a>
        </li>
        @endhasanyrole
        <li>
          <a class="dropdown-button" href="#!" data-activates="dropdown1">{{ Auth::user()->name }}
            <i class="material-icons right">arrow_drop_down</i>
          </a>
        </li>
      @endif
    </ul>
  </div>
</nav>
<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
  <li><a href="{{ url('/') }}/profile">My Account</a></li>
  <li><a href="{{ url('/') }}/mydocuments">My Documents</a></li>
  @hasanyrole('Root|Admin')
  <li><a href="{{ url('/') }}/dashboard/settings">Theme Settings</a></li>
  @endhasanyrole

  <li>
      <a href="{{ route('logout') }}"
          onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();">
          Logout
      </a>

      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
      </form>
  </li>
</ul>
