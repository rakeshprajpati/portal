@if(count($errors) > 0)
  @foreach($errors->all() as $error)
    <script>
      Materialize.toast("{{ $error }}",3000)
    </script>
  @endforeach
@endif

@if(session('success'))
  <script>
    Materialize.toast("{{ @session('success') }}",3000);
  </script>
@endif

@if(session('error'))
  <script>
      Materialize.toast("{{ @session('error') }}",3000);
  </script>
@endif
