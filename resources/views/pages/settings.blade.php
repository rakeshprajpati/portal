@extends('layouts.app')
@section('content')
<style>
.colorfield{
    width: 100px !important;
    float: right !important;
  }
</style>
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    <div class="col m11 s12">
      <div class="row">
     <!--    <h3 class="flow-text"><i class="material-icons">folder</i> Upload Document
          <a href="/guest/create" class="btn waves-effect waves-light right tooltipped" data-position="left" data-delay="50" data-tooltip="Upload New Document"><i class="material-icons">file_upload</i> New</a>
        </h3> -->
        <div class="divider"></div>
      </div>
      <div class="row">
        <div class="col m8 s12">

          {!! Form::open(['action' => 'SettingsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'col s12']) !!}
            {{ csrf_field() }}
          <div class="card hoverable">
            <div class="card-content">
              <div class="input-field">
                <i class="material-icons prefix">description</i>
                  {{ Form::text('top_header_logo_text', $data->top_header_logo_text ? $data->top_header_logo_text : ' '  ,['class' => 'validate', 'id' => 'top_header_logo_text']) }}
                <label for="description">Top Header Logo Text</label>
                 {{ Form::hidden('setting_id', $data->id ,['class' => 'validate', 'id' => 'setting_id']) }}

                
              </div>
              <br>
               <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('top_header_color', $data->top_header_color ? $data->top_header_color : '#0dc4da',['class' => 'colorfield', 'id' => 'top_header_logo_text']) }}
                <label for="top_header_color">Top Header Color</label>
                
              </div>
              <br>
              <br>
               <div class="file-field input-field">
                <div class="btn white">
                  <span class="black-text">Choose Header Logo</span>
                  {{ Form::file('logo',['id'=>'logo', 'accept'=>'image/*']) }}
                  @if ($errors->has('logo'))
                    <span class="red-text"><strong>{{ $errors->first('logo') }}</strong></span>
                  @endif
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
              <br>
              <div class="file-field input-field">
                <div class="btn white">
                  <span class="black-text">Choose Footer Logo </span>
                  {{ Form::file('footerlogo',['id'=>'footerlogo','accept'=>'image/*']) }}
                  @if ($errors->has('footerlogo'))
                    <span class="red-text"><strong>{{ $errors->first('footerlogo') }}</strong></span>
                  @endif
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
              <br>

              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('dashboard_box_color', $data->top_header_color ? $data->top_header_color : '#0dc4da' ,['class' => 'colorfield', 'id' => 'dashboard_box_color']) }}
                <label for="message_to_admin">Dashboard Box Color</label>
                
              </div>
              <br>

              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('sub_footer_color',$data->sub_footer_color ? $data->sub_footer_color : '#0dc4da',['class' => 'colorfield', 'id' => 'sub_footer_color']) }}
                <label for="sub_footer_color">Sub Footer Color</label>
                
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('copyright_footer_color' ,$data->copyright_footer_color ? $data->copyright_footer_color : '#0dc4da',['class' => 'colorfield', 'id' => 'copyright_footer_color']) }}
                <label for="copyright_footer_color">Copyright Footer Color</label>
              
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('button_color', $data->button_color ? $data->button_color : '#0dc4da' ,['class' => 'colorfield', 'id' => 'button_color']) }}
                <label for="button_color">Button Color</label>
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::color('button_hover_color' ,$data->button_hover_color ? $data->button_hover_color : '#0dc4da',['class' => 'colorfield', 'id' => 'button_hover_color']) }}
                <label for="button_hover_color">Button Hover Color</label>
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                <label for="button_hover_color">Notice Board Background Color</label>
                {{ Form::color('noticeboard_color' ,$data->noticeboard_color ? $data->noticeboard_color : '#f5de73',['class' => 'colorfield', 'id' => 'noticeboard_color']) }}
              </div>

              <br>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">description</i>
                {{ Form::text('noticeboard_heading',$data->noticeboard_heading ? $data->noticeboard_heading : ' ',['class' => 'validate', 'id' => 'noticeboard_heading']) }}
                <label for="noticeboard_heading">Notice Board Heading</label>
                @if ($errors->has('noticeboard_heading'))
                  <span class="red-text"><strong>{{ $errors->first('noticeboard_heading') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">description</i>
                {{ Form::textarea('noticeboard',$data->noticeboard ? $data->noticeboard : ' ',['class' => 'validate', 'id' => 'noticeboard','rows' => 10, 'cols' => 54, 'style' => 'resize:none; height:200px;']) }}
                <label for="noticeboard">Notice Board Message</label>
                @if ($errors->has('noticeboard'))
                  <span class="red-text"><strong>{{ $errors->first('noticeboard') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <p class="center">
                  {{ Form::submit('Save',['class' => 'btn-large waves-effect waves-light']) }}
                </p>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
        <div class="col m4 hide-on-med-and-down">
          <div class="card-panel teal">
            <h4>{{ $data->noticeboard_heading ? $data->noticeboard_heading : ' ' }}</h4>
            <p>
              <ul>
                <li>
                  {{ $data->noticeboard ? $data->noticeboard : ' ' }}
                </li>
              </ul>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
