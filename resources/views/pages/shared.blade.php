@extends('layouts.app')

@section('content')
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    <div class="col m11 s12">
      <div class="row">
        <h3 class="flow-text"><i class="material-icons">share</i> Shared Documents</h3>
        <div class="divider"></div>
      </div>
      <div class="card">
        <div class="card-content">
          <table class="bordered centered highlight responsive-table" id="myDataTable">
            <thead>
              <tr>
                  <th>File Name</th>
                  <th>Owner</th>
                  <th>Department</th>
                  <th>Uploaded At</th>
                  <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @if(count($shared) > 0 && !empty($shared))
                @foreach($shared as $share)
                <tr>
                  <td>{{ $share->name }}</td>
                  <td>{!! !empty($share->user->name) ? $share->user->name : 'User not exist' !!}</td>
                  <td>{!! !empty($share->user->department['dptName']) ? $share->user->department['dptName'] : 'Department not exist' !!}</td>
                  <td>{{ $share->created_at->toDayDateTimeString() }}</td>
                  <td>
                      <!-- <a href="{{ url('/') }}/share/open/{{ $share->id }}" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Open"><i class="material-icons">open_with</i></a> -->
                        @can('download')

                        <a href="{{url('/') }}/share/download/{{ $share->id }}"  class="tooltipped" data-position="left" data-delay="50" data-tooltip="Download"><i class="material-icons">file_download</i></a>
                        @endcan

                        <!-- DELETE using link -->
                        {!! Form::open(['action' => ['ShareController@destroy', $share->id],
                        'method' => 'DELETE', 'id' => 'form-delete-documents-' . $share->id]) !!}
                        @can('delete')
                        <a href="" class="data-delete tooltipped" data-position="left" data-delay="50" data-tooltip="Delete" data-form="documents-{{ $share->id }}"><i class="material-icons">delete</i></a>
                        @endcan
                        {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="6"><h5 class="teal-text">No Document has been uploaded</h5></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
