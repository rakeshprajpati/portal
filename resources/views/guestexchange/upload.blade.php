@extends('layouts.app')

@section('content')
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    <div class="col m11 s12">
      <div class="row">
     <!--    <h3 class="flow-text"><i class="material-icons">folder</i> Upload Document
          <a href="/guest/create" class="btn waves-effect waves-light right tooltipped" data-position="left" data-delay="50" data-tooltip="Upload New Document"><i class="material-icons">file_upload</i> New</a>
        </h3> -->
        <div class="divider"></div>
      </div>
      <div class="row">
        <div class="col m8 s12">
          {!! Form::open(['action' => 'GuestExchangeController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'col s12']) !!}
            {{ csrf_field() }}
          <div class="card hoverable">
            <div class="card-content">

              <!-- Recipient email info start -->
              <div class="input-field">
                <i class="material-icons prefix">account_circle</i>
                {{ Form::text('recipient_name','',['class' => 'validate', 'id' => 'recipient_name']) }}
                <label for="recipient_name">Recipient Name</label>
                @if ($errors->has('recipient_name'))
                  <span class="red-text"><strong>{{ $errors->first('recipient_name') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">email</i>
                  {{ Form::email('email','',['class' => 'validate', 'id' => 'email']) }}
                <label for="email">Email</label>
                @if ($errors->has('email'))
                  <span class="red-text"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">description</i>
                {{ Form::text('message_to_admin','',['class' => 'validate', 'id' => 'message_to_admin']) }}
                <label for="message_to_admin">Message to Admin</label>
                @if ($errors->has('message_to_admin'))
                  <span class="red-text"><strong>{{ $errors->first('message_to_admin') }}</strong></span>
                @endif
              </div>
              <br>
              <!-- Recipient email info end -->

              <div class="input-field">
                <i class="material-icons prefix">folder</i>
                {{ Form::text('name','',['class' => 'validate', 'id' => 'name']) }}
                <label for="name">File Name</label>
                @if ($errors->has('name'))
                  <span class="red-text"><strong>{{ $errors->first('name') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <i class="material-icons prefix">description</i>
                {{ Form::text('description','',['class' => 'validate', 'id' => 'description']) }}
                <label for="description">Description</label>
                @if ($errors->has('description'))
                  <span class="red-text"><strong>{{ $errors->first('description') }}</strong></span>
                @endif
              </div>
              <br>
              <div class="input-field">
                <!-- <input type="checkbox" id="isExpire" name="isExpire" checked/> -->
                {{ Form::checkbox('isExpire',1,true,['id' => 'isExpire']) }}
                <label for="isExpire">Does Not Expire</label>
              </div>
              <br>
              <div class="input-field">
                <!-- <input type="text" class="datepicker" name="expires_at" id="expirePicker" disabled> -->
                {{ Form::text('expires_at', '',['class' => 'datepicker', 'id' => 'expirePicker', 'disabled']) }}
                <label for="expirePicker">Expires At</label>
              </div>
              <br>
              <div class="file-field input-field">
                <div class="btn white">
                  <span class="black-text">Choose File (Max: 50MB)</span>
                  {{ Form::file('file[]', ['multiple'=>'multiple', 'id'=>'file']) }}
                  @if ($errors->has('file'))
                    <span class="red-text"><strong>{{ $errors->first('file') }}</strong></span>
                  @endif
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
              <br>
              <div class="input-field">
                <p class="center">
                  {{ Form::submit('Upload',['class' => 'btn-large waves-effect waves-light']) }}
                </p>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
       @if( \App\Http\Controllers\SettingsController::getSettings() )
        <div class="col m4 hide-on-med-and-down">
          <div class="card-panel teal">
            <h4>{{ \App\Http\Controllers\SettingsController::getSettings()->noticeboard_heading }}</h4>
            <p>
              <ol>
                <li>{{ \App\Http\Controllers\SettingsController::getSettings()->noticeboard }}</li>                
              </ol>
            </p>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
