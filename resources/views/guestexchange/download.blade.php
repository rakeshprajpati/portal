@extends('layouts.app')

@section('content')
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    <div class="col m11 s12">
      <div class="row">
     <!--    <h3 class="flow-text"><i class="material-icons">folder</i> Upload Document
          <a href="/guest/create" class="btn waves-effect waves-light right tooltipped" data-position="left" data-delay="50" data-tooltip="Upload New Document"><i class="material-icons">file_upload</i> New</a>
        </h3> -->
        <div class="divider"></div>
      </div>
      <div class="row">
        <div class="col m8 s12">
         {!! Form::open(['action' => 'GuestExchangeController@DownloadFiles', 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'col s12']) !!}
            {{ csrf_field() }}
          <div class="card hoverable">
            <div class="card-content">
              <!-- Recipient email info start -->
              <div class="input-field">
                <i class="material-icons prefix">file_download</i>
                {{ Form::text('download_code','',['class' => 'validate', 'id' => 'download_code']) }}
                <label for="download_code">Enter Download Code</label>
                @if ($errors->has('download_code'))
                  <span class="red-text"><strong>{{ $errors->first('download_code') }}</strong></span>
                @endif
              </div>
              <br>              
              <div class="input-field">
                <p class="center">
                  {{ Form::submit('Download',['class' => 'btn-large waves-effect waves-light']) }}
                </p>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
        @if( \App\Http\Controllers\SettingsController::getSettings() )
        <div class="col m4 hide-on-med-and-down">
          <div class="card-panel teal">
            <h4>{{ \App\Http\Controllers\SettingsController::getSettings()->noticeboard_heading }}</h4>
            <p>
              <ol>
                <li>{{ \App\Http\Controllers\SettingsController::getSettings()->noticeboard }}</li>                
              </ol>
            </p>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
