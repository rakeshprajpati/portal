@extends('layouts.app')

@section('content')
<style>
  .card-content2 {
    padding: 10px 7px;
  }
  /* --- for right click menu --- */
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
  .task i {
    color: orange;
    font-size: 35px;
  }
  /* context-menu */
  .context-menu {
    padding: 0 5px;
    margin: 0;
    background: #f7f7f7;
    font-size: 15px;
    display: none;
    position: absolute;
    z-index: 10;
    box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);
  }
  .context-menu--active {
    display: block;
  }
  .context-menu_items {
    margin: 0;
  }
  .context-menu_item {
    border-bottom: 1px solid #ddd;
    padding: 12px 30px;
  }
  .context-menu_item:last-child {
    border-bottom: none;
  }
  .context-menu_item:hover {
    background: #fff;
  }
  .context-menu_item i {
    margin: 0;
    padding: 0;
  }
  .context-menu_item p {
    display: inline;
    margin-left: 10px;
  }
  .unshow {
    display: none;
  }
</style>
<div class="row">
  <div class="section">
    <div class="col m1 hide-on-med-and-down">
      @include('inc.sidebar')
    </div>
    @hasanyrole('Root|Admin')

    <div class="col m11 s12">
      <div class="row">
        <h3 class="flow-text"><i class="material-icons">folder</i> Guest Exchange Documents
          <button class="btn red waves-effect waves-light right tooltipped delete_all" data-url="{{ url('guestdocumentsDeleteMulti') }}" data-position="left" data-delay="50" data-tooltip="Delete Selected Documents"><i class="material-icons">delete</i></button>
        </h3>
        <div class="divider"></div>
      </div>
      <div class="card z-depth-2">
        <div class="card-content">        
          <!-- TABLE View -->
          <div id="tableView">
            <div class="row">
              <table class="bordered centered highlight responsive-table" id="myDataTable">
                <thead>
                  <tr>
                      <th></th>
                      <th>File Name</th>
                      <th>Recipient Name</th>
                      <th>Email</th>
                      <th>Admin Message</th>
                      <th>Download Code</th>
                      <th>Uploaded At</th>
                      <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($docs) > 0)
                    @foreach($docs as $doc)
                    <tr id="tr_{{$doc->id}}">
                      <td>
                        <input type="checkbox" id="chk_{{ $doc->id }}" class="sub_chk" data-id="{{$doc->id}}">
                        <label for="chk_{{ $doc->id }}"></label>
                      </td>
                      <td>{{ $doc->name }}</td>
                      <td>{{ $doc->recipient_name }}</td>
                      <td>{{ $doc->email }}</td>
                      <td><a href="javascript:void()" class="tooltipped" data-tooltip="{{ $doc->message_to_admin }}" data-position="left" data-delay="50">{{ \App\Http\Controllers\GuestExchangeController::strip_tags_func($doc->message_to_admin,15) }}</a></td>
                      <td>{{ $doc->download_code }}</td>
                      <td>{{ $doc->created_at->toDayDateTimeString() }}</td>
                      <td>
                        <!-- download using link -->

                        @can('download')

                        <a href="{{url('/')}}/download/{{ $doc->id }}"  class="tooltipped" data-position="left" data-delay="50" data-tooltip="Download"><i class="material-icons">file_download</i></a>
                        @endcan
                        
                        <!-- DELETE using link -->
                        {!! Form::open(['action' => ['GuestExchangeController@destroy', $doc->id],
                        'method' => 'DELETE', 'id' => 'form-delete-documents-' . $doc->id]) !!}
                        @can('delete')
                        <a href="" class="data-delete tooltipped" data-position="left" data-delay="50" data-tooltip="Delete" data-form="documents-{{ $doc->id }}"><i class="material-icons">delete</i></a>
                        @endcan
                        {!! Form::close() !!}
                      </td>
                    </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="6"><h5 class="teal-text">Document Not Available</h5></td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endhasanyrole()

  </div>
</div>
@endsection
