<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group( ['middleware' => ['auth']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    // Route::resource('posts', 'PostController');
	Route::resource('permissions','PermissionController');
});

Route::get('/',function() {
    // display index page
    if (Auth::check()) {
      return view('/dashboard');
    }
    return view('auth.login');
});


//
Auth::routes();

// dashboard
Route::get('dashboard', function() {
  return view('/dashboard');
})->middleware('auth');

Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index');


// users
Route::resource('users','UsersController');
// departments
Route::resource('departments','DepartmentsController');
// categories
Route::resource('categories','CategoriesController');
Route::delete('categoriesDeleteMulti', 'CategoriesController@deleteMulti');
// documents
Route::resource('documents','DocumentsController');
Route::get('documents/download/{id}','DocumentsController@download');
Route::get('documents/open/{id}','DocumentsController@open');
Route::get('mydocuments','DocumentsController@mydocuments');
Route::get('/trash','DocumentsController@trash');
Route::get('documents/restore/{id}','DocumentsController@restore');
Route::delete('documentsDeleteMulti','DocumentsController@deleteMulti');
// search
Route::post('/search','DocumentsController@search');
// sort
Route::post('/sort', 'DocumentsController@sort');
// shared
Route::resource('shared','ShareController');
Route::get('share/{id}/upload','ShareController@upload');
Route::get('share/{id}/documents','ShareController@adminUserSharedDocs');
Route::get('share/open/{id}','ShareController@open');

Route::post('share/download/', ['as' => 'share.download', 'uses' => 'ShareController@DownloadFiles']);

// roles and permissions
Route::resource('roles','RolesController');
// profile
Route::resource('profile','ProfileController');
Route::patch('profile','ProfileController@changePassword');
// registeration requests
Route::resource('requests','RequestsController');
// backup
Route::get('backup','BackupController@index');
Route::get('backup/create','BackupController@create');
Route::get('backup/download','BackupController@download');
Route::get('backup/delete','BackupController@delete');
// log
Route::get('logs','LogController@log');
Route::get('logsdel','LogController@logdel');
// Guest Exchange
Route::get('guest_exchange/upload/', 'GuestExchangeController@index')->name('guest_exchange.upload');
Route::get('guest_exchange/download/', 'GuestExchangeController@downloads')->name('guest_exchange.download');
Route::post('guest_exchange/download/', ['as' => 'guest_exchange.download', 'uses' => 'GuestExchangeController@DownloadFiles']);

Route::get('guest_exchange/list/', 'GuestExchangeController@listDocuments')->name('guest_exchange.list')->middleware('auth');

Route::resource('guest_exchange','GuestExchangeController');
Route::delete('guestdocumentsDeleteMulti','GuestExchangeController@deleteMulti');


// dashboard settings

Route::get('dashboard/settings/', 'SettingsController@index')->middleware('auth');
Route::resource('settings','SettingsController');

use Illuminate\Http\Request;

Route::get('download/{id}','DownloadController@guest_multi_down')->middleware('auth');

Route::get('share/download/{id}','DownloadController@shared_multi_down')->middleware('auth');

Route::get('document/download/{id}','DownloadController@document_multi_down')->middleware('auth');

Route::get('/postDownload',function (Request $request){

    $url=$request->urlpath;
    $pathToFile = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix($url);

    if(file_exists($pathToFile)){

      return response()->download($pathToFile);
    }else{
      return Redirect::back()->withErrors('File not exists!');
    }

});